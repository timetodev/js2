var studentsJSON = JSON.parse (`{
    "students": [
        {
            "firstname": "Alexandre",
            "lastname": "Briffaut",
            "sex": "M"
        },
        {
            "firstname": "Béatrice",
            "lastname": "Bécue",
            "sex": "F"
        },
        {
            "firstname": "Benoît",
            "lastname": "Delobel",
            "sex": "M"
        },
        {
            "firstname": "Émeric",
            "lastname": "Glinkowski",
            "sex": "M"
        },
        {
            "firstname": "Florian",
            "lastname": "Thelliez",
            "sex": "M"
        },
        {
            "firstname": "Gwenaëlle",
            "lastname": "Lohard",
            "sex": "F"
        },
        {
            "firstname": "Jérémy",
            "lastname": "Théry",
            "sex": "M"
        },
        {
            "firstname": "Johan",
            "lastname": "Fievet",
            "sex": "M"
        },
        {
            "firstname": "Justine",
            "lastname": "Moreau",
            "sex": "F"
        },
        {
            "firstname": "Justine",
            "lastname": "Piebois",
            "sex": "F"
        },
        {
            "firstname": "Justine",
            "lastname": "Telmann",
            "sex": "F"
        },
        {
            "firstname": "Kévin",
            "lastname": "Lebrun",
            "sex": "M"
        },
        {
            "firstname": "Loïc",
            "lastname": "Noisette",
            "sex": "M"
        },
        {
            "firstname": "Mathieu",
            "lastname": "Castel",
            "sex": "M"
        },
        {
            "firstname": "Matthias",
            "lastname": "Dacquignie",
            "sex": "M"
        },
        {
            "firstname": "Rémi",
            "lastname": "Ponche",
            "sex": "M"
        },
        {
            "firstname": "Samuel",
            "lastname": "Poudroux",
            "sex": "M"
        },
        {
            "firstname": "Sébastien",
            "lastname": "Jurdeczka",
            "sex": "M"
        },
        {
            "firstname": "Sullivan",
            "lastname": "Delaby",
            "sex": "M"
        },
        {
            "firstname": "Thomas",
            "lastname": "Gérard",
            "sex": "M"
        },
        {
            "firstname": "Tiffany",
            "lastname": "Deschauwer",
            "sex": "F"
        },
        {
            "firstname": "Valentin",
            "lastname": "Misiaszek",
            "sex": "M"
        },
        {
            "firstname": "Yann",
            "lastname": "Duchateau",
            "sex": "M"
        }
    ]
}
`);
// We're having a look at our variable
console.log(studentsJSON);

// We display each student
studentsJSON.students.forEach(student => {
    var studentCard = document.createElement("div");
    studentCard.className  = "card";
    studentCard.style.width = "18rem";
    document.querySelector("main").appendChild(studentCard);

    // Image src 
    var studentCardImg = document.createElement("img");
    studentCardImg.src = 
    "https://via.placeholder.com/180x50";
    studentCardImg.alt = "Ma super image";
    studentCardImg.className = "card-img-top";
    studentCard.appendChild(studentCardImg);
 

    // card body div
    var studentCardBody = document.createElement("div");
    studentCardBody.className  = "card-body";
    studentCard.appendChild(studentCardBody);


    // title h5 
    var studentCardH5 = document.createElement("h5");
    studentCardH5.className  = "card-title";
    studentCardH5.innerHTML = `${student.firstname} ${student.lastname}`;
    studentCardBody.appendChild(studentCardH5);


    // paragraphe add 
    var studentCardP = document.createElement("p");
    studentCardP.className  = "card-text";
    studentCardBody.appendChild(studentCardP);

    // add href link 
    var studentCardA = document.createElement("a");
    studentCardA.href = "#";
    studentCardA.innerHTML = "Mon super lien";
    studentCardA.className  = "btn btn-primary";
    studentCardBody.appendChild(studentCardA);


    
})


