var studentsJSON = {};

fetch(`http://api-students.popschool-lens.fr/students.json`)
    .then(response => response.json())
    .then(myJSON =>{
        console.log(myJSON)
        studentsJSON = myJSON;
        studentsJSON.students.forEach(student => {
            var studentCard = document.createElement("div");
            studentCard.className  = "card";
            studentCard.style.width = "18rem";
            document.querySelector("main").appendChild(studentCard);
        
            // Image src 
            var studentCardImg = document.createElement("img");
            studentCardImg.src = 
            "https://via.placeholder.com/180x50";
            studentCardImg.alt = "Ma super image";
            studentCardImg.className = "card-img-top";
            studentCard.appendChild(studentCardImg);
         
        
            // card body div
            var studentCardBody = document.createElement("div");
            studentCardBody.className  = "card-body";
            studentCard.appendChild(studentCardBody);
        
        
            // title h5 
            var studentCardH5 = document.createElement("h5");
            studentCardH5.className  = "card-title";
            studentCardH5.innerHTML = `${student.firstname} ${student.lastname}`;
            studentCardBody.appendChild(studentCardH5);
        
        
            // paragraphe add 
            var studentCardP = document.createElement("p");
            studentCardP.className  = "card-text";
            studentCardBody.appendChild(studentCardP);
        
            // add href link 
            var studentCardA = document.createElement("a");
            studentCardA.href = "#";
            studentCardA.innerHTML = "Mon super lien";
            studentCardA.className  = "btn btn-primary";
            studentCardBody.appendChild(studentCardA);
        
        
            
        })
    })
    .catch( error => console.log(error))

    